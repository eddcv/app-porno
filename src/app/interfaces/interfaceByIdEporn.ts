
export interface VideoDetalle {
    id?:            string;
    title?:         string;
    keywords?:      string;
    views?:         number;
    rate?:          string;
    url?:           string;
    added?:         string;
    length_sec?:    number;
    length_min?:    string;
    embed?:         string;
    default_thumb?: Thumb;
    thumbs?:        Thumb[];
}

export interface Thumb {
    size:   Size;
    width:  number;
    height: number;
    src:    string;
}

export enum Size {
    Medium = "medium",
}
