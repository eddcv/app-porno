import { TestBed } from '@angular/core/testing';

import { EpoRestServiceService } from './epo-rest-service.service';

describe('EpoRestServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EpoRestServiceService = TestBed.get(EpoRestServiceService);
    expect(service).toBeTruthy();
  });
});
