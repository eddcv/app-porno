import { TestBed } from '@angular/core/testing';

import { RedTubeServiceService } from './red-tube-service.service';

describe('RedTubeServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RedTubeServiceService = TestBed.get(RedTubeServiceService);
    expect(service).toBeTruthy();
  });
});
