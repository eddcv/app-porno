import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { TopLevel } from '../interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { VideoDetalle } from '../interfaces/interfaceByIdEporn';

@Injectable({
  providedIn: 'root'
})
export class EpoRestServiceService {

  private newestPage =0;
  constructor(private http:HttpClient) { }


  searchAllPorno(){

    this.newestPage ++;
    const queryPage = `&page=${ this.newestPage }`
    return this.http.get<TopLevel>(environment.uriEpornoDomain+environment.uriEpornoContextSearch+queryPage);
  }

  getByIdEporn(idVideo:string){
    const queryPage = `&page=${ this.newestPage }`
    return this.http.get(environment.uriEpornoDomain+environment.uriEpornoContextById+idVideo);
  }
  
}
