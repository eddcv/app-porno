import { Component, OnInit, Output } from '@angular/core';
import { EpoRestServiceService } from '../_service/epo-rest-service.service';
import { Video } from '../interfaces/interfaces';
import { RedTubeServiceService } from '../_service/red-tube-service.service';
import { VideoVideo, VideoElement } from '../interfaces/interfaceRedTube';
import { EventEmitter } from 'events';
import { DetalleVideoComponent } from '../componentes/detalle-video/detalle-video.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  videosList: Video[] = [];
  nuevosRtList: VideoElement[] = [];
  slidesOpts = {
    slidesPerView: 1.1
  };
  @Output() cargarMas = new EventEmitter();
  constructor(private epoRestSerice:EpoRestServiceService, private redTubeService: RedTubeServiceService,
    private modalCtrl:ModalController) {
    
  }

  ngOnInit(){
    //this.getNewsRedTube();
    this.getAllEporno();
  }

  getAllEporno(){
    this.epoRestSerice.searchAllPorno()
    .subscribe(response =>{
      console.log('Response', response);
      this.videosList = response.videos;
    });
  }

  getNewsRedTube(){
    this.getLista();
  }

  cargaMas(){
    this.getLista();
  }

  getLista(){
    this.epoRestSerice.searchAllPorno()
    .subscribe(response =>{
      console.log('Response', response);
      const temp = [...this.videosList, ...response.videos];
      this.videosList = temp;
    });
  }

  async verDetalleVideo(id:string){
    const modal = await this.modalCtrl.create({
      component: DetalleVideoComponent,
      componentProps:{
        id:id
      }
    });
    modal.present();
  }
}
