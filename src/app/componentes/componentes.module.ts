import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetalleVideoComponent } from './detalle-video/detalle-video.component';



@NgModule({
  entryComponents:[DetalleVideoComponent],
  // declarations: [DetalleVideoComponent],
  exports:[
    //DetalleVideoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentesModule { }
