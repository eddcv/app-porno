import { Component, OnInit, Input } from '@angular/core';
import { EpoRestServiceService } from 'src/app/_service/epo-rest-service.service';
import { VideoDetalle } from 'src/app/interfaces/interfaceByIdEporn';
import { ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detalle-video',
  templateUrl: './detalle-video.component.html',
  styleUrls: ['./detalle-video.component.scss'],
})
export class DetalleVideoComponent implements OnInit {
  @Input("id")id;
  videot: VideoDetalle={};
  iframe:any;
  videoEmbed: string;
  constructor(private eporestService: EpoRestServiceService, private modalCtrl: ModalController, private sanitizer:DomSanitizer) { }
  
  ngOnInit() {
    this.getDetalleVideo();
    // this.getEmbededVideo();
  }

  getDetalleVideo(){
    this.eporestService.getByIdEporn(this.id)
    .subscribe((resp:VideoDetalle) => {
      this.videot=resp;
      console.log(this.videot);
    });
  }
  
  regresar(){
    this.modalCtrl.dismiss();
  }
}
