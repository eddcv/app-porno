import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagenPipe } from './imagen.pipe';
import { SafePipe } from './safe.pipe';



@NgModule({
  declarations: [ImagenPipe, SafePipe],
  exports:[
    ImagenPipe, SafePipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
