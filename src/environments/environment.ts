// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  uriEpornoDomain:'https://www.eporner.com',
  uriEpornoContextSearch: '/api/v2/video/search/?query=mature&per_page=10&thumbsize=big&order=top-weekly&gay=0&lq=1&format=json',
  uriEpornoContextById: '/api/v2/video/id/?format=jsoni&id=',
  uriRedTubeDomain:'https://api.redtube.com/',
  uriRedTubeContextSearchNews:'?data=redtube.Videos.searchVideos&output=json&thumbsize=medium&ordering=newest'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
